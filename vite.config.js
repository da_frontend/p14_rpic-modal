import { resolve } from 'node:path';

import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';
// https://vitejs.dev/config/
export default defineConfig((configEnv) => ({
    plugins: [react()],
    build: {
        lib: {
            entry: resolve('src', 'components/RpicModal.jsx'),
            name: 'rpic-modal',
            formats: ['es', 'umd'],
            fileName: (format) => `rpic-modal.${format}.js`,
        },
    },
}));

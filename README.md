# rpic-modal

## What is it?

This is just a react modal customizable by props

## Install

```bash
npm i rpic-modal
```

## Usage

After installing the package, go to the parent component of your modal and import useState

```jsx
import { useState } from 'react';
```

Then use a state to show / hide you modal, some App code exemple:

```jsx
import { useState } from 'react';
import RpicModal from './components/RpicModal';

function App() {
    const [showModal, setShowModal] = useState(false);

    return (
        <div className="App">
            <div onClick={() => setShowModal(!showModal)}>Show the modal</div>
            {showModal && (
                <RpicModal
                    content="Customizable content"
                    closeModal={() => setShowModal(!showModal)}
                />
            )}
        </div>
    );
}
```

## Props

| PropName        |   Type   |                       Exemple |
| --------------- | :------: | ----------------------------: |
| content         |  string  |   'This is a content exemple' |
| width           |  string  |                        '50px' |
| height          |  string  |                         '50%' |
| backgroundColor |  string  |                         'red' |
| border          |  string  |             '2px solid black' |
| borderRadius    |  string  |                        '10px' |
| justifyContent  |  string  |       'start', 'end', center' |
| alignItems      |  string  |       'start', 'end', center' |
| iconSize        |  string  |                        '30px' |
| closeModal      | function | () => { console.log('close')} |

## License

MIT

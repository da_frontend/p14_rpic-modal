import closeIcon from './close.svg';

/**
 * RpicModal
 * @param {string} content - Content of the modal
 * @param {string} width - Width of the modal
 * @param {string} height - Height of the modal
 * @param {string} backgroundColor - Background color of the modal
 * @param {string} border - Border of the modal (shape and color)
 * @param {string} borderRadius - Border radius of the modal
 * @param {string} justifyContent - Use to position modal on horizontal axe (start, center, end)
 * @param {string} alignItems - Use to position modal on vertical axe (start, center, end)
 * @param {string} iconSize - Define close icon size in modal
 * @param {function} closeModal - Callback dispatch on click on close icon
 * @returns JSX Modal
 */
export default function RpicModal({
    content,
    width = '300px',
    height = '200px',
    backgroundColor = 'white',
    border = '2px solid white',
    borderRadius = '10px',
    justifyContent = 'center',
    alignItems = 'center',
    iconSize = '30px',
    closeModal,
}) {
    return (
        <div
            id="modalWrapper"
            style={{
                position: 'fixed',
                display: 'flex',
                top: 0,
                left: 0,
                width: '100%',
                minHeight: '100vh',
                justifyContent: justifyContent,
                alignItems: alignItems,
                backgroundColor: 'rgba(0, 0, 0, 0.8)',
                zIndex: '9999',
            }}
        >
            <div
                id="modalContentWrapper"
                style={{
                    position: 'relative',
                    display: 'flex',
                    flexDirection: 'column',
                    width: width,
                    height: height,
                    maxHeight: '80vh',
                    backgroundColor: backgroundColor,
                    border: border,
                    borderRadius: borderRadius,
                    boxShadow: '7px 5px 5px rgba(0,0,0,.4)',
                }}
            >
                <div
                    id="modalIconWrapper"
                    style={{
                        display: 'flex',
                        justifyContent: 'end',
                        padding: '5px',
                    }}
                >
                    <img
                        style={{
                            width: iconSize,
                            cursor: 'pointer',
                        }}
                        src={closeIcon}
                        alt="close"
                        onClick={() => {
                            closeModal();
                        }}
                    />
                </div>
                <div
                    id="modalContent"
                    style={{
                        height: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '5px',
                        flexWrap: 'wrap',
                        marginBottom: '40px',
                        paddingInline: '20px',
                    }}
                >
                    {content}
                </div>
            </div>
        </div>
    );
}

import './App.css';
import { useState } from 'react';
import RpicModal from './components/RpicModal';

function App() {
    const [showModal, setShowModal] = useState(false);

    return (
        <div className="App">
            <div onClick={() => setShowModal(!showModal)}>Show the modal</div>
            {showModal && (
                <RpicModal
                    content="Customizable content"
                    width="50%"
                    height="200px"
                    border="2px solid white"
                    borderRadius="10px"
                    justifyContent="center"
                    alignItems="center"
                    iconSize="20px"
                    closeModal={() => setShowModal(!showModal)}
                />
            )}
        </div>
    );
}

export default App;
